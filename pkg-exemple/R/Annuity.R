### pkg: <package title>
###
### Computation of the present value of immediate and
### due annuity.
###
### Reference: Kellison, S. G. (1991), The Theory of Interest,
### Irwin/McGraw-Hill, ISBN 978-025609150-2
###
### AUTHORS: Jean-Christophe Langlois, Vincent Goulet <vincent.goulet@act.ulaval.ca>
### LICENSE: GPL 2 or later.

pres.value <- function(n, i, due = FALSE)
{
    stopifnot(exprs = {
        "interest rate must be positive" = all(i >= 0)
        "number of periods must be strictly positive" = all(n > 0)
    })

    ## We need to treat specially the case where i = 0 since the
    ## general formula does not apply.
    ##
    ## First, create a vector to hold present values.
    value <- numeric(max(length(n), length(i)))

    ## Set the present value for cases with i = 0.
    w <- i == 0
    value[w] <- n

    ## Now the cases with i > 0.
    w <- !w                             # reverse positions
    iw <- i[w]                          # needed interest rates
    nw <- n[w]                          # needed durations
    value[w] <- (1 - 1/(1 + iw)^nw)/iw

    if (due)
        value[w] <- (1 + iw) * value[w]

    value
}
