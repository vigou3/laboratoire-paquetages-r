===
=== Conception de paquetages R
===

# 2024.08 (2024-08-27)

## Changements

- Nouvelle image de couverture.
- Modifications d'ordre cosmétique au texte.


# 2024.01 (2024-01-13)

## Changements

- Correction d'une coquille dans le texte.
- Changements cosmétiques à la page des notices.


# 2023.01 (2023-01-05)

## Changements

- Modification du nom du projet.
- Répertoire `man`: le fait que les exemples des rubriques d'aide
  doivent être valides est davantage mis en évidence.


# 2022.03 (2022-03-02)

## Nouveautés

- Fichier `DESCRIPTION`: indication d'indenter les lignes du champ
  *Description* après la première ligne.

## Changements

- Fichier `DESCRIPTION`: les exemples des divers champs réutilisent
  les valeurs de l'exemple principal en début de section.
- Révisions et améliorations diverses au texte.


# 2021.03-1 (2021-03-19)

## Nouveautés

- Répertoire `tests`: mention explicite qu'il faut charger le
  paquetage dans les tests.
- Construction et validation d’un paquetage: diapositive sur une
  erreur fréquente sous Windows, soit un chemin d'accès vers le
  paquetage qui contient des lettres accentuées.

## Changements

- Fichier `DESCRIPTION`: correction d'une utilisation de «Licence»
  plutôt que «License» dans l'exemple d'utilisation du champ du même
  nom.
- Aller plus loin: réorganisation des puces.


# 2021.03 (2021-03-15)

Version initiale.
