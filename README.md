<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Conception de paquetages R

[Conception de paquetages R](https://vigou3.gitlab.io/conception-paquetages-r) est un laboratoire (ou atelier) consacré à la création de paquetages simples pour le système [R](https://r-project.org). Le laboratoire est offert dans le cadre du cours [ACT-2002 Méthodes numériques en actuariat](https://www.ulaval.ca/les-etudes/cours/repertoire/detailsCours/act-2002-methodes-numeriques-en-actuariat.html) de l'[Université Laval](https://ulaval.ca).

L'activité consiste en une présentation entrecoupée d'exemples et d'exercices.

Le répertoire `pkg-exemple` contient une ébauche (très avancée) de paquetage.

## Auteurs

Vincent Goulet, professeur titulaire, [École d'actuariat](https://www.act.ulaval.ca), [Université Laval](https://ulaval.ca), avec la collaboration de Jean-Christophe Langlois

## Licence

«Conception de paquetages R» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Modèle de développement

Le processus de rédaction et de maintenance du projet suit le modèle [*Gitflow Workflow*](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow ). Seule particularité: la branche *master* se trouve dans le dépôt [`conception-paquetages-r`](https://gitlab.com/vigou3/conception-paquetages-r) dans GitLab, alors que la branche de développement se trouve dans le dépôt [`conception-paquetages-r-devel`](https://projets.fsg.ulaval.ca/git/scm/vg/conception-paquetages-r-devel) dans le serveur BitBucket de la Faculté des sciences et de génie de l'Université Laval.

Prière de passer par le dépôt `conception-paquetages-r-devel` pour proposer des modifications; consulter le fichier `CONTRIBUTING.md` pour la marche à suivre.
