%%% Copyright (C) 2024 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «Conception de paquetages R»
%%% https://gitlab.com/vigou3/conception-paquetages-r
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,french]{beamer}
  \usepackage{babel}
  \usepackage[babel=true]{microtype}
  \usepackage[autolanguage]{numprint}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{fontawesome5}              % various icons
  \usepackage[overlay,absolute]{textpos} % page de titre
  \usepackage{upquote}                   % accents droits dans code
  \usepackage{dirtree}                   % arbre de fichiers
  \usepackage{relsize}
  \usepackage{fancyvrb}                  % code source
  \usepackage{pict2e}                    % pyramide de la comm.
  \usepackage{metalogo}                  % logo \XeLaTeX

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{Utilisation et conception \\ de paquetages R \\ et
    d'interfaces API}
  \author{Introduction}
  \date{}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Thème Beamer. Personnalisation pour le tableau lumineux.
  \usetheme[background=dark,numbering=none]{metropolis}
  \setbeamercolor{normal text}{fg=white, bg=black}
  \setbeamercolor{frametitle}{fg=alert, bg=black}
  \setbeamersize{text margin right=90mm}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]

  %% Page de titre
  \renewcommand{\titlepage}{%
    \begin{textblock*}{60mm}[0.5,1](40mm,90mm)
      \includegraphics[width=50mm]{Anthophora_montana}
    \end{textblock*}
    \begin{textblock*}{60mm}(10mm,25mm)
      \larger[2]\bfseries \inserttitle
    \end{textblock*}
    \begin{textblock*}{60mm}(10mm,48mm)
      \insertauthor
    \end{textblock*}}

  %% Couleurs
  \colorlet{alert}{mLightBrown}
  \colorlet{code}{mLightGreen}
  \definecolor{good}{rgb}{0,0.7,0}         % vert «avantage»
  \definecolor{bad}{rgb}{0.7,0,0}          % rouge «inconvénient»
  %% palette Okabe-Ito
  \definecolor{OIGold}{RGB}{230,159,0}     % orange
  \definecolor{OISkyBlue}{RGB}{86,180,233} % bleu clair
  \definecolor{OIGreen}{RGB}{0,158,115}    % vert émeraude
  \definecolor{OIYellow}{RGB}{240,228,66}  % jaune
  \definecolor{OIBlue}{RGB}{0,114,178}     % bleu
  \definecolor{OIRed}{RGB}{213,94,0}       % rouge vermillion
  \definecolor{OIPink}{RGB}{204,121,167}   % rose pourpre

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Hyperlien avec symbole de lien externe juste après
  \newcommand{\link}[2]{%
      \href{#1}{#2~\raisebox{0.1ex}{\smaller\faExternalLink*}}}

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}
  \newcommand{\pkg}[1]{\textbf{#1}}

\begin{document}

%% frontmatter
\maketitle

%% mainmatter
\begin{frame}[plain]
  Vous souhaitez faire profiter \\
  \alert{le plus grand nombre} \\
  de vos connaissances et de vos \\
  découvertes de manière \\
  \alert{efficace} et \alert{reproductible} \\
  \pause
  $+$ \\
  favoriser la \alert{réutilisation} \\
  et l'\alert{innovation}
\end{frame}

\begin{frame}
  \frametitle{Pyramide de la communication \\ en science des données}

  \smaller[2]
  \setlength{\unitlength}{5mm}
  \begin{picture}(12,9)
    \thicklines
    {
      \color{OIGold}
      \polygon*(0,0)  (0.9,1.35)(11.1,1.35)(12,0)
      \color{OISkyBlue}
      \polygon*(1,1.5)(1.9,2.85)(10.1,2.85)(11,1.5)
      \color{OIGreen}
      \polygon*(2,3.0)(2.9,4.35)( 9.1,4.35)(10,3.0)
      \color{OIPink}
      \polygon*(3,4.5)(3.9,5.85)( 8.1,5.85)( 9,4.5)
      \color{OIBlue}
      \polygon*(4,6.0)(4.9,7.35)( 7.1,7.35)( 8,6.0)
      \color{OIRed}
      \polygon*(5,7.5)(6,9)(7,7.5)
    }

    \polygon(0,0)  (0.9,1.35)(11.1,1.35)(12,0)
    \polygon(1,1.5)(1.9,2.85)(10.1,2.85)(11,1.5)
    \polygon(2,3.0)(2.9,4.35)( 9.1,4.35)(10,3.0)
    \polygon(3,4.5)(3.9,5.85)( 8.1,5.85)( 9,4.5)
    \polygon(4,6.0)(4.9,7.35)( 7.1,7.35)( 8,6.0)
    \polygon(5,7.5)(6,9)(7,7.5)

    \put(6,0.72){\makebox(0,0){Personne à personne}}
    \put(6,2.12){\makebox(0,0){Rapports \emph{ad hoc}}}
    \put(6,3.62){\makebox(0,0){Rapports programmés}}
    \put(6,5.12){\makebox(0,0){Apps}}
    \put(6,6.62){\makebox(0,0){Paquetages}}
    \put(6,8.02){\makebox(0,0){API}}
  \end{picture}
\end{frame}

\begin{frame}
  \frametitle{Paquetage}

  {
    \larger[3]
    \begin{tabular}{ccc>{\arraybackslash\centering}p{1em}c}
      \faUserCog &
      \faLongArrowAltRight &
      \faBox &
      \makebox[0pt]{\raisebox{ 0.5ex}{\faLongArrowAltRight}}%
      \makebox[0pt]{\raisebox{-0.5ex}{\faLongArrowAltLeft}} &
      \faUser
    \end{tabular}
  }
  \medskip
  \begin{itemize}
  \item[\textcolor{good}{\faCheckCircle[regular]}] Grande flexibilité
  \item[\textcolor{good}{\faCheckCircle[regular]}] Innovation
  \item[\textcolor{bad}{\faTimesCircle[regular]}]  Gestion des versions
  \item[\textcolor{bad}{\faTimesCircle[regular]}]  Spécifique à un langage
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Interface API}

  {
    \larger[3]
    \begin{tabular}{ccc>{\arraybackslash\centering}p{1em}c}
      \faUserCog &
      \faLongArrowAltRight &
      \faServer &
      \makebox[0pt]{\raisebox{ 0.5ex}{\faLongArrowAltRight}}%
      \makebox[0pt]{\raisebox{-0.5ex}{\faLongArrowAltLeft}} &
      \faDesktop
    \end{tabular}
  }
  \medskip
  \begin{itemize}
  \item[\textcolor{good}{\faCheckCircle[regular]}] Très grande flexibilité
  \item[\textcolor{good}{\faCheckCircle[regular]}] Universel
  \item[\textcolor{bad}{\faTimesCircle[regular]}]  Mise en œuvre et utilisation
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth]{8h7r4p}
\end{frame}

\begin{frame}
  \frametitle{Paquetage}

  Ensemble cohérent de fonctions, \\
  de bases de données et de documentation
  \begin{itemize}
  \item diffusion à large échelle
  \item intégrité du code
  \item accès à la documentation
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Structure d'un paquetage R}

  \smaller
  \dirtree{%
    .1 \code{pkg/}.
      .2  \code{DESCRIPTION}.
      .2  \code{NAMESPACE}.
      .2  \code{R/}.
        .3 \code{foo.R}.
      .2 \code{man/}.
        .3 \code{foo.Rd}.
      .2 \code{tests/}.
        .3 \code{tests-foo.R}. }
\end{frame}

\begin{frame}
  \frametitle{Interface API}

  \larger[4]
  \begin{center}
    \faDesktop
    \qquad
    \makebox[0pt]{\raisebox{ 0.5ex}{\faLongArrowAltRight}}%
    \makebox[0pt]{\raisebox{-0.5ex}{\faLongArrowAltLeft}}%
    \visible<2>{%
      \makebox[0pt]{\raisebox{ 2.5ex}{\small\code{2, 2}}}}%
    \visible<2->{%
      \makebox[0pt]{\raisebox{-1.5ex}{\small\code{4}}}}%
    \visible<3>{%
      \makebox[0pt]{\raisebox{ 2.5ex}{\small\code{https://example.com/api/add/2/2}}}}
    \qquad
    \makebox[0pt]{\faServer}%
    \visible<2>{\makebox[0pt]{\raisebox{2.5ex}{\small\code{+}}}}%
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Outils de travail}

  \begin{itemize}[<+->]
  \item \textbf{Utilisation}: utilitaire curl depuis la ligne de
    commande Unix
  \item \textbf{Conception}: paquetage \pkg{plumber} dans R
  \item \textbf{Déploiement}: serveur Linux
  \end{itemize}
\end{frame}

\begin{frame}

\end{frame}

%% backmatter
\begin{frame}[plain]
  \smaller[3] %
  Ce document a été produit par le système de mise en page {\XeLaTeX}
  avec la classe \textbf{beamer} et le thème Metropolis. Les titres et
  le texte sont composés en Fira~Sans, le code informatique en
  Fira~Mono. Les icônes proviennent de la police Font~Awesome. Les
  illustrations ont été entièrement réalisées avec {\LaTeX}.
\end{frame}

\end{document}

%%% Local Variables:
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
